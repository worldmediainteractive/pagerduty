const request = require('request');
const { RateLimiter } = require('limiter');

const limiter = new RateLimiter(1, 'second');

const PagerService = {
  /**
   * @param { String } title
   * @param { (String|Error) } details
   * @param { String } incidentKey
   * @param { String } serviceID
   */
  sendIncident: (title, details, incidentKey, serviceID) => {
    const { PAGERDUTY_API_KEY, NODE_ENV } = process.env;

    if (PAGERDUTY_API_KEY && title && details && incidentKey && serviceID && NODE_ENV !== 'test') {
      limiter.removeTokens(1, () => {
        const options = {
          method: 'POST',
          json: true,
          url: 'https://api.pagerduty.com/incidents',
          headers: {
            'Cache-Control': 'no-cache',
            From: 'techteam@worldmedia.net',
            Accept: 'application/vnd.pagerduty+json;version=2',
            Authorization: `Token token=${PAGERDUTY_API_KEY}`,
          },
          body: {
            incident: {
              type: 'incident',
              title,
              service: {
                id: serviceID,
                type: 'service_reference',
              },
              incident_key: incidentKey,
              body: {
                type: 'incident_body',
                details,
              },
            },
          },
        };

        request(options, (error) => {
          if (error) {
            const err = new Error(error);
            console.error(err);
          }
        });
      });
    } else if (!title) {
      const err = new Error('Pagerduty title variable is not defined');
      console.error(err);
    } else if (!details) {
      const err = new Error('Pagerduty details variable is not defined');
      console.error(err);
    } else if (!incidentKey) {
      const err = new Error('Pagerduty incidentKey variable is not defined');
      console.error(err);
    } else if (!serviceID) {
      const err = new Error('Pagerduty serviceID variable is not defined');
      console.error(err);
    } else if (!PAGERDUTY_API_KEY) {
      const err = new Error('Pagerduty API key variable is not defined');
      console.error(err);
    } else if (NODE_ENV !== 'test') {
      console.error('Running in test mode. NOT calling Pagerduty');
    }
  },
};

module.exports = PagerService;
